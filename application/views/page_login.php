<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/main.css">
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <title>Vali Admin</title>
    </head>
    
    
    <?php
    $login = array(
            'name'	=> 'login',
            'id'	=> 'login',
            'value' => set_value('login'),
            'maxlength'	=> 80,
            'size'	=> 30,
    );
    if ($login_by_username AND $login_by_email) {
            $login_label = 'Email or login';
    } else if ($login_by_username) {
            $login_label = 'Login';
    } else {
            $login_label = 'Email';
    }
    $password = array(
            'name'	=> 'password',
            'id'	=> 'password',
            'size'	=> 30,
    );
    $remember = array(
            'name'	=> 'remember',
            'id'	=> 'remember',
            'value'	=> 1,
            'checked'	=> set_value('remember'),
            'style' => 'margin:0;padding:0',
    );

    ?>
    
    <body>
        <section class="material-half-bg">
            <div class="cover"></div>
        </section>
        <section class="login-content">
            <div class="logo">
                <h1>Housing</h1>
            </div>
            <div class="login-box">
                <form class="login-form" method="POST" action="">
                    <h3 class="login-head"><i class="fa fa-lg fa-fw fa-user"></i>SIGN IN</h3>
                    <div class="form-group">
                        <?php
                        if (isset($errorMessage)) {
                            echo "<span class='text-danger'>" . $errorMessage . "</span>";
                        }
                        ?>
                        <label class="control-label">USERNAME</label>
                        <input class="form-control" type="text" name="login" placeholder="Email" autofocus>
                        <span class="text-danger"><?php echo form_error($login['name']); ?><?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?></span>
                    </div>
                    <div class="form-group">
                        <label class="control-label">PASSWORD</label>
                        <input class="form-control" type="password" name="password" placeholder="Password">
                        <span class="text-danger"><?php echo form_error($password['name']); ?><?php echo isset($errors[$password['name']])?$errors[$password['name']]:''; ?></span>
                    </div>
                    <div class="form-group">
                        <div class="utility">
                            <div class="animated-checkbox">
                                <label class="semibold-text">
                                    <input type="checkbox"><span class="label-text">Stay Signed in</span>
                                </label>
                            </div>
                            <p class="semibold-text mb-0"><a data-toggle="flip">Forgot Password ?</a></p>
                        </div>
                    </div>
                    <div class="form-group btn-container">
                        <button class="btn btn-primary btn-block"><i class="fa fa-sign-in fa-lg fa-fw"></i>SIGN IN</button>
                    </div>
                </form>
                <form class="forget-form" action="index.html">
                    <h3 class="login-head"><i class="fa fa-lg fa-fw fa-lock"></i>Forgot Password ?</h3>
                    <div class="form-group">
                        <label class="control-label">EMAIL</label>
                        <input class="form-control" type="text" placeholder="Email">
                    </div>
                    <div class="form-group btn-container">
                        <button class="btn btn-primary btn-block"><i class="fa fa-unlock fa-lg fa-fw"></i>RESET</button>
                    </div>
                    <div class="form-group mt-20">
                        <p class="semibold-text mb-0"><a data-toggle="flip"><i class="fa fa-angle-left fa-fw"></i> Back to Login</a></p>
                    </div>
                </form>
            </div>
        </section>
    </body>
    <script src="<?= base_url() ?>assets/js/jquery-2.1.4.min.js"></script>
    <script src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>assets/js/plugins/pace.min.js"></script>
    <script src="<?= base_url() ?>assets/js/main.js"></script>
</html>