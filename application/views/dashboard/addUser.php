<div class="row">

    <div class="col-md-6">
        <div class="card">
            <h3 class="card-title">Register</h3>            
            <div class="card-body">
                <form class="form-horizontal" method="POST" action="">
                    <div class="form-group">
                        <label class="control-label col-md-3">Name</label>
                        <div class="col-md-8">
                            <input name="name" class="form-control" type="text" placeholder="Enter full name">
                            <span class="text-danger"><?= form_error('name');?></span>
                        </div>
                    </div>                                       
                    <div class="form-group">
                        <label class="control-label col-md-3">User name</label>
                        <div class="col-md-8">
                            <input name="username" class="form-control" type="text" placeholder="User name">
                            <span class="text-danger"><?= form_error('username');?></span>
                        </div>
                    </div>                                       
                    <div class="form-group">
                        <label class="control-label col-md-3">Email</label>
                        <div class="col-md-8">
                            <input name="email" class="form-control col-md-8" type="email" placeholder="Enter email address">
                            <span class="text-danger"><?= form_error('email');?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Password</label>
                        <div class="col-md-8">
                            <input name="password" class="form-control" type="password" placeholder="Password">
                            <span class="text-danger"><?= form_error('password');?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Confirm Password</label>
                        <div class="col-md-8">
                            <input name="c_password" class="form-control" type="password" placeholder="Confirm Password">
                            <span class="text-danger"><?= form_error('c_password');?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Address</label>
                        <div class="col-md-8">
                            <textarea name="address" class="form-control" rows="4" placeholder="Address"></textarea>
                            <span class="text-danger"><?= form_error('address');?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Gender</label>
                        <div class="col-md-9">
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="gender" value="Male">Male
                                </label>
                            </div>
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="gender" value="Female">Female
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Identity Proof</label>
                        <div class="col-md-8">
                            <input name="image" class="form-control" type="file">
                            <span class="text-danger"><?= form_error('image');?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-3">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox">I accept the terms and conditions
                                </label>
                            </div>
                        </div>
                    </div>                
            <div class="card-footer">
                <div class="row">
                    <div class="col-md-8 col-md-offset-3">
                        <button class="btn btn-primary icon-btn" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Register</button>&nbsp;&nbsp;&nbsp;<a class="btn btn-default icon-btn" href="#"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
                    </div>
                </div>
            </div>
            </form>
            </div>
        </div>
    </div>    
</div>