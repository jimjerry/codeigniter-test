<?php

Class Login extends CI_Controller {

    public function index() {
        $this->form_validation->set_rules('email', 'Email', 'required|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|trim');

        if ($this->form_validation->run()) {
            $email = $this->input->post('email');
            $password = md5($this->input->post('password'));

            $attr = array(
                'email' => $email,
                'password' => $password
            );
            $table = 'login';
            $select = '*';
            $result = $this->General_model->userLoginDataCheck($table, $attr, $select);
            // echo $this->db->last_query();exit;
            if ($result) {
                $this->session->set_userdata($array);
                redirect("Login/dashboard");
            }

            $data['errorMessage'] = 'Username/ Password is not correct';
            $this->load->view('page_login', $data);
        }
        $this->load->view('page_login');
    }

    public function dashboard() {
        $data['loadpage'] = 'dashboard/home';
        $this->load->view('dashboard/dashboard_layout', $data);
    }

}

?>