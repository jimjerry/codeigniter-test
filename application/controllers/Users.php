<?php

    Class Users extends CI_Controller{
        public function addUser(){
            
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('username', 'username', 'required|is_unique[users.username]');
            $this->form_validation->set_rules('email', 'Email', 'required|is_unique[users.email]');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('c_password', 'Confirm Password', 'required|matches[password]');
            $this->form_validation->set_rules('address', 'Address', 'required');
            $this->form_validation->set_rules('gender', 'Gender', 'required');
            $this->form_validation->set_rules('image', 'Image', 'required');
            
            if ($this->form_validation->run()){
                $name = $this->input->post('name');
                $username = $this->input->post('username');
                $email = $this->input->post('email');
                $password = $this->input->post('password');
                $gender = $this->input->post('gender');
                $image = $this->input->post('image');
                
                $attr = array(
                    'name' => $name,
                    'username' => $username,
                    'email' => $email,
                    'password' => $password,
                    'gender' => $gender,
                    'image' => $image
                );
                $table = 'users';
                $result = $this->General_model->inserUser($table, $attr);
                // echo $this->db->last_query();exit;
                
                if($result){
                    redirect('Users/allUsers');
                }else{
                    redirect('Auth');
                }
            }
            
            $data['addUser'] = 'dashboard/addUser';
            $this->load->view('dashboard/dashboard_layout', $data);
        }
        
        public function allUsers(){
            
            $select = 'username, email, `name`, gender';
            $from = 'users';
            $data['result'] = $this->General_model->allUsers($select, $from);
            //print_r($result); exit();
            
            $data['allUsers'] = 'dashboard/allUsers';
            $this->load->view('dashboard/dashboard_layout', $data);
        }
    }
    
?>