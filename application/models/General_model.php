<?php

Class General_model extends CI_Model{

    public function userLoginDataCheck($table, $attr, $select)
	{

        $this->db->select($select);
        $this->db->from($table);
        if ($attr) {
            $this->db->where($attr);
        }
        $data = $this->db->get();
        if ($data->num_rows() > 0) {
            return $data->row();
        } else {
            return false;
        }
    }
    
    public function inserUser($table, $attr)
	{
        $this->db->insert($table, $attr);  
        
        if($this->db->insert_id()){
            return $this->db->insert_id();
        }else{
            return FALSE;
        }
        
    }
    
    public function allUsers($select, $from)
	{
        $this->db->select($select);
        $this->db->from($from);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }	

}

?>